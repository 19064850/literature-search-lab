




































































 





 





























































	




 
	













































































 





 

























































	




 
	
































---
author: Dr. John Noll
institute: University of Hertfordshire
course: 7COM1085
term: spring-21
date: \today
module_title: #module_tile
...
---
title: Literature Search lab
subtitle: 7COM1085 -- Research Methods
module: 7COM1085
...

---
author: Dr. John Noll
date: Friday, 30 Apr, 2021
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview


This deliverable focuses on the actual literature search part of your
review.  You will execute your search using the search
string you developed for the last deliverable, download the results, then apply your
inclusion and exclusion criteria.

# Instructions


1.  Read Section 6.2 "Study Selection" of Kitchenham &
    Charters' "Guidelines for performing systematic literature reviews
    in software engineering".

2.  Read Part B, item 3 "Literature search" of the
    coursework specification, to be sure you understand the requirements
    for this part of the coursework.

3.  Clone your group's Bitbucket repository.

4. Revise your _research_question.txt_ and/or _review_protocol.txt_
   to account for any feedback you have received.
   
4.  Execute your (revised) search in _IEEEXplore_, using the "Command
Search" field of the Advanced Search facility.

5.  Download the results as a CSV file, using the _Export_ dropdown.

6.  Copy the CSV file into your repository, as _papers.csv_.
_IMPORTANT:_ you _must_ name the file _papers.csv_ exactly, else our
marking scripts will not find it, and you will receive **ZERO CREDIT
FOR THIS DELIVERABLE**.

7.  Add _five_ column names to _papers.csv_, as follows:

    * Column AE: "include title"
    * Column AF: "include abstract"
    * Column AG: "exclude"
    * Column AH: "accept contents"
    * Column AI: "rq answers"

    See below for an example.

    ![papers.csv](papers.csv.png) \

8. Compare the title of _each_ and _every_ paper in _papers.csv_ to
your inclusion criteria.  If the title indicates the paper clearly
does not meet your _inclusion criteria_, write "no" in column AE
("include title").  Conversely, if the title indicates the paper meets
your inclusion criteria, or you are not sure, write "yes" in the
"include title" column.

9. Compare the abstract of _each_ and _every_ paper in _papers.csv_,
where there is a "yes" in the "include title" column (_AE_), to
your inclusion criteria.  If the abstract indicates the paper clearly
does not meet your _inclusion criteria_, write "no" in column _AF_
("include abstract").  Conversely, if the abstract indicates the paper meets
your inclusion criteria, or you are not sure, write "yes" in the
"include abstract" column.

    Here are the allowable combinations of answers in these two
    columns:

    include title   include abstract
   --------------- ------------------
         yes           yes
         yes           no
         no            no
         no          (blank)

    The following combinations of answers in these two
    columns would be incorrect:

    include title   include abstract
   --------------- ------------------
         yes         (blank)
         no            yes
       (blank)         yes
       (blank)         no
       (blank)       (blank)



10. Commit _papers.csv_, and your revised _review_protocol.txt_ and/or
_research_question.txt_, files by the 23:59 Friday,
2021-04-30.
